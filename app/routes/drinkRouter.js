//import { Express } from "express";
const express = require("express");

const drinkMiddleware = require("../middlewares/drink.middleware");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", drinkMiddleware.getAllDrinksMiddlewares,(req, res) => {
    res.json({
        message: "Get all drinks"
    })
})

router.post("/", drinkMiddleware.createDrinksMiddlewares,(req, res) => {
    res.json({
        message: "Create a drink"
    })
})

router.get("/:drinkId", drinkMiddleware.getDetailDrinksMiddlewares,(req, res) => {
    res.json({
        message: "Get detail drink"
    })
})

router.put("/:drinkId", drinkMiddleware.updateDrinksMiddlewares,(req, res) => {
    res.json({
        message: "update drinks"
    })
})

router.delete("/:drinkId", drinkMiddleware.deleteDrinksMiddlewares,(req, res) => {
    res.json({
        message: "delete drinks"
    })
})

module.exports = router;