//import { Express } from "express";
const express = require("express");

const voucherMiddleware = require("../middlewares/voucher.middleware");

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", voucherMiddleware.getAllVouchersMiddlewares,(req, res) => {
    res.json({
        message: "Get all Vouchers"
    })
})

router.post("/", voucherMiddleware.createVouchersMiddlewares,(req, res) => {
    res.json({
        message: "Create a Voucher"
    })
})

router.get("/:VoucherId", voucherMiddleware.getDetailVouchersMiddlewares,(req, res) => {
    res.json({
        message: "Get detail Voucher"
    })
})

router.put("/:VoucherId", voucherMiddleware.updateVouchersMiddlewares,(req, res) => {
    res.json({
        message: "update  Vouchers"
    })
})

router.delete("/:VoucherId", voucherMiddleware.deleteVouchersMiddlewares,(req, res) => {
    res.json({
        message: "Delete Vouchers"
    })
})

module.exports = router;